## Autoget Nexflow
Nextflow implementation of Autoget using individual containers (MarDB backend processing) excluding download, which is handled elsewhere.

### Dependencies
nextflow 21.10

Running with trace and/or nextflow tower requires procps/gcc installed in all containers. As per Dec 2021, ecogenomic/gtdbtk:1.7.0 does not have this installed by default.
To add this, make a dockerfile containing these lines:  
```
FROM ecogenomic/gtdbtk:1.7.0
RUN apt-get update && apt install -y procps g++ 
```
Build with:  
```
docker build -t "tag" .
```
where "tag" is arbitrary. Then set your newly built container in the nextflow.config file instead of ecogenomic/gtdbtk:1.7.0


### Usage Example
Make sure you have an input folder containing fastafiles with the extension ".fa" for all files! (Assembly files from NCBI should have this extension by default).  
Correct reference data path to gtdbtk needs to be set in nextflow.config.  
CPU/threads/forks can also be changed if preferred. The current configuration is optimized for local backend machines and uses a maximum of ~550-600 memory, where all tools have about the same runtime pr. genome.  
```
nextflow run main.nf --dir <inputdir>
```

### Troubleshooting  
#### Segmentation faults running large jobs (5k+ genomes)
The way nextflow stages jobs can be problematic for large jobs depending of the topology of the pipeline. Too many asyncronously submitted jobs at the same time might cause a stack overflow. To avoid/alleviate this problem, set stack size to an arbitrary high number or unlimited using ulimit before starting the Nextflow workflow. (This is also hardcoded in the parser container since it has a very high number of arguments to the python-script, which also depends on stack size) 
```
ulimit -s unlimited
```

#### WARN: Failed to publish file
This is a known issue using certain Nextflow versions for big/complex workflow topologies.  
Version 21.10.4 is verified to work with this workflow. You can specify a specific nextflow version to use at runtime, and it will be downlaoded on the fly:
```
NXF_VER=21.10.4 nextflow main.nf --dir <dir>
```
