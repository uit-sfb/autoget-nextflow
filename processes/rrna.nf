process Rrna5s {

  tag 'infernal=1.1.4'

  input:
  path inputfile
  path cm

  output:
  path "${inputfile}.5s.txt", emit: rrna

  shell:
  '''
  cmsearch --cpu 1 -Z 1000 --cut_ga --tblout !{inputfile}.5s.txt !{cm} !{inputfile}
  '''
}

process Rrna16s {

  tag 'infernal=1.1.4'

  input:
  path inputfile
  path cm

  output:
  path "${inputfile}.16s.txt", emit: rrna

  shell:
  '''
  cmsearch --cpu 1 -Z 1000 --cut_ga --tblout !{inputfile}.16s.txt !{cm} !{inputfile}
  '''
}

process Rrna23s {

  tag 'infernal=1.1.4'

  input:
  path inputfile
  path cm

  output:
  path "${inputfile}.23s.txt", emit: rrna

  shell:
  '''
  cmsearch --cpu 1 -Z 1000 --cut_ga --tblout !{inputfile}.23s.txt !{cm} !{inputfile}
  '''
}
