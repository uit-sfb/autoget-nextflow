process Prokka {

  publishDir "${params.publish_dir}/output/prokka", mode: 'copy'
  container 'quay.io/biocontainers/prokka:1.14.6--pl5262hdfd78af_1'
  tag 'prokka=1.14.6'

  input:
  path inputfile

  output:
  path "${inputfile.getBaseName()}", emit: dir
  path "${inputfile.getBaseName()}/${inputfile.getBaseName()}.log", emit: log

  shell:
  '''
  prokka --force --cpu 5 --outdir !{inputfile.getBaseName()} --prefix !{inputfile.getBaseName()} !{inputfile}
  '''
}
