process Parser {

  container 'amancevice/pandas:1.3.4'
  publishDir "${params.publish_dir}/output/", mode: 'copy'
  tag "parser=1.0"

  input:
  path parser
  path checkm
  path gtdbtk
  path trnas
  path gc
  path stats
  path rrna5s
  path rrna16s
  path rrna23s
  path prokka

  output:
  path "out.tsv", emit: parsed

  shell:
  '''
  # This is need due to an absurd amount of arguments with big runs. ("unlimited" is not a supported argument for containeroptions="--ulimit x")
  ulimit -s unlimited 
  python !{parser} !{prokka} !{checkm} !{gtdbtk} !{trnas} !{gc} !{stats} !{rrna5s} !{rrna16s} !{rrna23s}
  '''
}
