process Seqkit {

  tag 'seqkit=2.1.0'

  input:
  path inputfile

  output:
  path "${inputfile}.stats.txt", emit: stats
  path "${inputfile}.gc.txt", emit: gc

  shell:
  """
  # Escaped metacharacters needs to be escaped, hence \\n
  grep -v "^>" !{inputfile} | awk 'BEGIN { ORS=""; print ">!{inputfile}\\n" } { print }' > concatenated_!{inputfile}
  seqkit stats -a !{inputfile} > !{inputfile}.stats.txt
  seqkit fx2tab --gc --name concatenated_!{inputfile} > !{inputfile}.gc.txt
  """

}
