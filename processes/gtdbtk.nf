process Gtdbtk {

  tag {"gtdbtk=1.7.0 - task: ${task.index}"}

  input:
  path inputdir

  output:
  path "gtdb", emit: dir
  //path "gtdb/classify/gtdbtk.*.summary.tsv", emit: results
  path "*.tsv", emit: results

  shell:
  '''
  # Inputfiles need to be renamed incase they are part of the gtdb database - which will cause the run to crash. Files are named back to their original names in parser
  ls -1 . | grep fa | xargs -I{} mv {} tmp{}
  gtdbtk classify_wf -x fa --genome_dir . --out_dir gtdb --cpus 20 --force --pplacer_cpus 1
  #Output tsvs also need unique renmaing pr task.index to not crash the parser
  cp gtdb/classify/gtdbtk.*.summary.tsv .
  ls -1 | grep tsv | xargs -I{} mv {} !{task.index}.{}  
  '''
}
