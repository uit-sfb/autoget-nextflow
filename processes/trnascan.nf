process Trnascan {

  tag "trnascan-se=2.0.7"

  input:
  path inputfile

  output:
  path "${inputfile}.trna.txt", emit: trna

  shell:
  '''
  tRNAscan-SE -q -B --thread 1 -o !{inputfile}.trna.txt !{inputfile}
  '''
}
