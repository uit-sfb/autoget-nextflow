process Rename {

  input:
  path inputdir

  output:
  path "renamed", emit: renamed

  shell:
  '''
  mkdir renamed
  ls -1 !{inputdir} | xargs -I{} cp !{inputdir}/{} renamed/.
  ls -1 renamed | xargs -I{} mv renamed/{} renamed/tmp{}
  '''
}

