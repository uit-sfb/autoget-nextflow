process Checkm {

  tag {"checkm-genome=1.1.3 - task: ${task.index}"}

  input:
  path inputdir

  output:
  path "checkm", emit: dir
  path "${task.index}.checkm.txt", emit: results

  shell:
  '''
  checkm lineage_wf -f !{task.index}.checkm.txt --pplacer_threads 1 -t 20 -x fa . checkm
  '''
}
