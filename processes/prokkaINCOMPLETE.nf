process Prokka {

  publishDir "${params.publish_dir}/output/", mode: 'copy'
  container 'quay.io/biocontainers/prokka:1.14.6--pl5262hdfd78af_1'
  tag 'prokka=1.14.6'

  input:
  path inputfile

  output:
  path "output/prokka/!{inputfile}", emit: dir
  //path "output/checkm/results.txt", emit: results

  shell:
  '''
  prokka --force --cpu !{params.cpu} --outdir output/prokka/!{inputfile} !{inputfile} 1>/dev/null 2>/dev/null
  checkm lineage_wf -f checkm/results.txt --pplacer_threads 4 -t !{params.cpu} -x fna !{inputdir} checkm 1>/dev/null 2>/dev/null
  '''
}
