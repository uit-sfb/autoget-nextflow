#!/usr/bin/env python3
import os, pandas, collections, sys

# Start with ying (results in data/)
outputdir = 'REMOVE'
parsed = collections.defaultdict(str)
inputfiles = sys.argv[1:]
counter = 0

for i,filename in enumerate(inputfiles):

    counter += 1

    # Special case for checkm (contains all runs in one file)
    if filename.endswith("checkm.txt"):
        with open(filename, "r") as result:
            data = result.readlines()
            data = data[3:]
            data = data[:-1]
            for line in data:
                line = line.split()
                inp = line[0].strip()+".fa"

                if not parsed[inp]:
                    parsed[inp] = collections.defaultdict(str)
                
                parsed[inp]['checkm_strain_heterogeneity'] = line[-1].strip()
                parsed[inp]['checkm_contamination'] = line[-2].strip()
                parsed[inp]['checkm_completeness'] = line[-3].strip()
        continue

    # Special case for gtdbtk Archaeaeaaeaeae (contains all runs in one file)
    if filename.endswith("gtdbtk.ar122.summary.tsv"):
        with open(filename, "r") as result:
            data = result.readlines()
            data = data[1:]
            for line in data:
                line = line.split("\t")
                # [3:] removes the string tmp, added to make sure gtdbtk does not crash (filenames that already exist in gtdbtk databases are not allowed for some reason)
                inp = line[0][3:].strip()+".fa"
                
                if not parsed[inp]:
                    parsed[inp] = collections.defaultdict(str)

                parsed[inp]['gtdb_classification'] = line[1].strip()
                parsed[inp]['gtdb_fastani_reference'] = line[2].strip()
                parsed[inp]['gtdb_fastani_ani'] = line[5].strip()
        continue

    # Specialcase for GTDBTK Bacteria (contains all runs in one file)
    if filename.endswith("gtdbtk.bac120.summary.tsv"):
        with open(filename, "r") as result:
            data = result.readlines()
            data = data[1:]
            for line in data:
                line = line.split("\t")
                # [3:] removes the string tmp, added to make sure gtdbtk does not crash (filenames that already exist in gtdbtk databases are not allowed for some reason)
                inp = line[0][3:].strip()+".fa"

                if not parsed[inp]:
                    parsed[inp] = collections.defaultdict(str)

                parsed[inp]['gtdb_classification'] = line[1].strip()
                parsed[inp]['gtdb_fastani_reference'] = line[2].strip()
                parsed[inp]['gtdb_fastani_ani'] = line[5].strip()
        continue

    # inp = original input filename
    # Special case for .log (single suffix, [-1])
#    inp = ('.').join(filename.split('.')[:-1])+'.fa'

#    if not parsed[inp]:
#        parsed[inp] = collections.defaultdict(str)

    if filename.endswith(".log"):
        inp = ('.').join(filename.split('.')[:-1])+'.fa'
        if not parsed[inp]:
            parsed[inp] = collections.defaultdict(str)
        with open(filename, "r") as result:
            data = result.readlines()
            data = [x for x in data if ('Found' in x) and ('CDS' in x)]
            parsed[inp]['CDS'] = data[0][17:][:4]
        continue

    # inp = original input filename
    # Special case for .tool.txt (double suffix, [-2])
    inp = ('.').join(filename.split('.')[:-2])

    if not parsed[inp]:
        parsed[inp] = collections.defaultdict(str)

    if filename.endswith("trna.txt"):
        with open(filename, "r") as result:
            data = result.readlines()
            if not data:
                continue
                #parsed[mmpid]['trnas_unique']=0
                #parsed[mmpid]['trnas_total']=0
            else:
                data = data[3:]
                #print (data)
                trnas = [x.split("\t")[4] for x in data]
                parsed[inp]['trnas_unique']=len(set(trnas))
                parsed[inp]['trnas_total']=len(trnas)

    if filename.endswith("stats.txt"):
        with open(filename, "r") as result:
            data = result.readlines()
            data = data[1].split()
            parsed[inp]['n50'] = data[12]
            parsed[inp]['num_seqs'] = data[3]
            parsed[inp]['sum_length'] = data[4]

    if filename.endswith("gc.txt"):
        with open(filename, "r") as result:
            data = result.read()
            parsed[inp]['gc'] = data.strip().split("\t")[-1]

    if filename.endswith("5s.txt"):
        with open(filename, "r") as result:
            data = result.readlines()
            data = [x for x in data if not x.startswith("#")]
            parsed[inp]['5s'] = len(data)
                
    if filename.endswith("16s.txt"):
        with open(filename, "r") as result:
            data = result.readlines()
            data = [x for x in data if not x.startswith("#")]
            parsed[inp]['16s'] = len(data)

    if filename.endswith("23s.txt"):
        with open(filename, "r") as result:
            data = result.readlines()
            data = [x for x in data if not x.startswith("#")]
            parsed[inp]['23s'] = len(data)


df = pandas.DataFrame(parsed)
#print(df.T.head)
df.T.to_csv("out.tsv", sep="\t")

