#!/usr/bin/env nextflow
// DSL 2 support and thread configuration. Default is 4 threads, provide --cpu on command line to change it. r2 defaults to null to supress warnings
nextflow.enable.dsl=2
params.publish_dir = './workflow'

// Include modules for each tool
include {Checkm} from './processes/checkm.nf'
include {Gtdbtk} from './processes/gtdbtk.nf'
include {Rename} from './processes/rename.nf'
include {Trnascan} from './processes/trnascan.nf'
include {Rrna5s; Rrna16s; Rrna23s} from './processes/rrna.nf'
include {Seqkit} from './processes/seqkit.nf'
include {Prokka} from './processes/prokka.nf'
include {Parser} from './processes/parser.nf'

workflow{
  // Input data. (dir for pipelines, files for individual tools)
  inputfiles = Channel.fromPath("${params.dir}/*")
  inputdir = Channel.fromPath("${params.dir}")

  // Define paths to Ribosomal rRNA CMs
  cm5s = Channel.value(file("${baseDir}/Rfams/5S_rfam.cm"))
  cm16s = Channel.value(file("${baseDir}/Rfams/16S_rfam.cm"))
  cm23s = Channel.value(file("${baseDir}/Rfams/23S_rfam.cm"))

  // Define path to parser
  parser = Channel.value(file("${baseDir}/autoget_parse_results.py"))
  
  // Tools to run
  Checkm(inputfiles.buffer( size: 100, remainder: true ))
  Trnascan(inputfiles)
  Rrna5s(inputfiles, cm5s)
  Rrna16s(inputfiles, cm16s)
  Rrna23s(inputfiles, cm23s)
  Seqkit(inputfiles)
  //Rename(inputdir)
  Gtdbtk(inputfiles.buffer( size: 500, remainder: true ))
  Prokka(inputfiles)

  // Parse and wrap up. 
  Parser(parser, Prokka.out.log.collect(), Checkm.out.results.collect(), Gtdbtk.out.results.collect(), Trnascan.out.trna.collect(), Rrna5s.out.rrna.collect(), Rrna16s.out.rrna.collect(), Rrna23s.out.rrna.collect(), Seqkit.out.gc.collect(), Seqkit.out.stats.collect())
}
